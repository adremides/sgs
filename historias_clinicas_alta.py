# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'historias_clinicas_alta.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_historias_clinicas_alta(object):
    def setupUi(self, historias_clinicas_alta):
        historias_clinicas_alta.setObjectName("historias_clinicas_alta")
        historias_clinicas_alta.resize(676, 472)
        font = QtGui.QFont()
        font.setPointSize(13)
        historias_clinicas_alta.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/salud.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        historias_clinicas_alta.setWindowIcon(icon)
        self.dte_fecha = QtWidgets.QDateEdit(historias_clinicas_alta)
        self.dte_fecha.setGeometry(QtCore.QRect(50, 90, 181, 30))
        self.dte_fecha.setStyleSheet("background-color: rgb(255, 255, 255);\n"
"selection-color: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);\n"
"selection-background-color: rgb(51, 153, 255);\n"
"font: 13pt \"MS Shell Dlg 2\";")
        self.dte_fecha.setDate(QtCore.QDate(1990, 1, 1))
        self.dte_fecha.setObjectName("dte_fecha")
        self.dte_fecha.setCalendarPopup(True)
        self.lbl_fecha = QtWidgets.QLabel(historias_clinicas_alta)
        self.lbl_fecha.setGeometry(QtCore.QRect(50, 50, 171, 30))
        self.lbl_fecha.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";")
        self.lbl_fecha.setObjectName("lbl_fecha")
        self.lbl_descripcion = QtWidgets.QLabel(historias_clinicas_alta)
        self.lbl_descripcion.setGeometry(QtCore.QRect(50, 190, 171, 30))
        self.lbl_descripcion.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";")
        self.lbl_descripcion.setObjectName("lbl_descripcion")
        self.txt_descripcion = QtWidgets.QTextEdit(historias_clinicas_alta)
        self.txt_descripcion.setGeometry(QtCore.QRect(50, 230, 571, 171))
        self.txt_descripcion.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";\n"
"background-color: rgb(255, 255, 255);")
        self.txt_descripcion.setObjectName("textEdit")

        self.retranslateUi(historias_clinicas_alta)
        QtCore.QMetaObject.connectSlotsByName(historias_clinicas_alta)

    def retranslateUi(self, historias_clinicas_alta):
        _translate = QtCore.QCoreApplication.translate
        historias_clinicas_alta.setWindowTitle(_translate("historias_clinicas_alta", "Gestión de Salud   |   Alta de historias clínicas"))
        self.lbl_fecha.setText(_translate("historias_clinicas_alta", "Fecha"))
        self.lbl_descripcion.setText(_translate("historias_clinicas_alta", "Descripción"))

