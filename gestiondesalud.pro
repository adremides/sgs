#-------------------------------------------------
#
# Project created by QtCreator 2018-08-25T20:37:21
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gestiondesalud
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui \
    form_pacientes_alta_modificacion.ui \
    historias_clinicas.ui \
    pacientes.ui \
    turnos.ui
