import os, sys, subprocess, datetime
from datetime import date
from w_historias_clinicas_alta import VentanaHistoriasClinicasAlta
from base_datos import Base
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QTableWidgetItem
from historias_clinicas import Ui_historias_clinicas
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import A4
from reportlab.lib.enums import TA_LEFT, TA_CENTER
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from ConfigGlobal import Ventana


# esta clase define el formato del pie de página del PDF generado con reportlab
class FooterCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)
        for page in self.pages:
            self.__dict__.update(page)
            self.draw_canvas(page_count)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_canvas(self, page_count):
        page = "Página %s de %s" % (self._pageNumber, page_count)
        x = 128
        self.saveState()
        self.setStrokeColorRGB(0, 0, 0)  # Color del borde de la línea
        self.setLineWidth(0.5)
        self.line(66, 78, A4[0] - 66, 78)
        pdfmetrics.registerFont(TTFont('Arial', 'Arial.ttf'))
        self.setFont('Arial', 9)
        self.drawString(A4[0]-x, 65, page)
        self.restoreState()


class VentanaHistoriasClinicas(QWidget):
    obj_hist_clin = Ui_historias_clinicas()
    lista_hist_clin = []
    nombre_paciente = ""
    id_paciente = -1
    agregar_hist_clin = None

    def __init__(self, lista_historias_clinicas, id_paciente):
        QWidget.__init__(self)
        self.obj_hist_clin.setupUi(self)

        Ventana(self)

        del self.lista_hist_clin[:]
        self.id_paciente = id_paciente
        self.nombre_paciente = Base.Paciente.get_paciente(id_paciente)
        # a mí no me aparce centrado en la pantalla, así que lo centro así
        self.centrar_widget()
        self.lista_hist_clin = lista_historias_clinicas
        self.cargar_lista_hist_clin()

        self.obj_hist_clin.tbl_lista_hist_clin.setColumnHidden(2, True)  # ocultar id_paciente
        if len(self.lista_hist_clin) < 1:
            self.obj_hist_clin.btn_ver_reporte.setEnabled(False)
        else:
            self.obj_hist_clin.btn_ver_reporte.setEnabled(True)
        self.obj_hist_clin.btn_agregar_hist_clin.clicked.connect(self.ventana_agregar_hist_clin)

        # Declaro acá la instancia de w_historias_clinicas_alta
        self.agregar_hist_clin = VentanaHistoriasClinicasAlta(self.id_paciente)
        # Para poder captar la señal enviada apenas se cierre la ventana de agregar historias clínicas
        # y así poder recargar la grilla ni bien haya terminado de hacer el insert en tabla historias_clinicas
        self.agregar_hist_clin.senial.connect(self.recargar_lista_hist_clin)

        self.obj_hist_clin.tbl_lista_hist_clin.verticalHeader().setVisible(False)
        self.obj_hist_clin.btn_ver_reporte.clicked.connect(self.imprimir_reporte)

    def recargar_lista_hist_clin(self):
        del self.lista_hist_clin[:]
        self.lista_hist_clin = Base.Historia_clinica.get_lista_hist_clin(self.id_paciente)
        self.cargar_lista_hist_clin()
        self.obj_hist_clin.tbl_lista_hist_clin.horizontalHeader().setStretchLastSection(True)

    def cargar_lista_hist_clin(self):
        while (self.obj_hist_clin.tbl_lista_hist_clin.rowCount() > 0):
            self.obj_hist_clin.tbl_lista_hist_clin.removeRow(0)
        # Cargo la grilla de historias clínicas
        if self.lista_hist_clin != False:
            for item in self.lista_hist_clin:
                rowPosition = self.obj_hist_clin.tbl_lista_hist_clin.rowCount()
                self.obj_hist_clin.tbl_lista_hist_clin.insertRow(rowPosition)
                self.obj_hist_clin.tbl_lista_hist_clin.setItem(rowPosition, 0,
                                                   QTableWidgetItem(str(item.fecha.strftime("%d/%m/%Y"))))
                self.obj_hist_clin.tbl_lista_hist_clin.setItem(rowPosition, 1, QTableWidgetItem(item.descripcion))
                self.obj_hist_clin.tbl_lista_hist_clin.setItem(rowPosition, 2, QTableWidgetItem(str(item.id_paciente)))
            self.obj_hist_clin.tbl_lista_hist_clin.resizeColumnsToContents()

    def centrar_widget(self):
        # qr = tamaño del QWidget
        qr = self.frameGeometry()
        # clase QDesktopWidget tiene información acerca del tamaño de la pantalla del usuario.
        # Así, puede determinar dónde está el centro y dónde poner el QWidget para que quede centrado en pantalla
        # cp = tamaño de la pantalla
        cp = QDesktopWidget().availableGeometry().center()
        # Mueve el centro de qr al centro de cp
        qr.moveCenter(cp)
        # mueve el widget arriba a la izquierda de su punto central, así esa ventana queda centrada en pantalla
        self.move(qr.topLeft())

    def ventana_agregar_hist_clin(self):
        self.agregar_hist_clin.show()

    def imprimir_reporte(self):
        documento_pdf = []
        styleSheet = getSampleStyleSheet()

        estilo_texto = ParagraphStyle('', fontSize=10, textColor='#000', rightIndent=0,alignment=TA_LEFT)
        estilo_tabla_encabezado = ParagraphStyle('', fontSize=10, alignment=TA_CENTER, spaceBefore=0,
                                            spaceAfter=0, textColor='#000', leftIndent=0)
        # creo un estilo para el titulo, le puse Titulo1 porque se lo inventé, se le puede poner otro nombre
        styleSheet.add(ParagraphStyle(name='Titulo1', alignment=TA_CENTER, fontSize=12, textColor='#000'))

        documento_pdf.append(Spacer(0, 50))

        titulo = '<font><b>Historia clínica del paciente ' + self.nombre_paciente + '</b></font>'

        documento_pdf.append(Paragraph(titulo, styleSheet["Titulo1"]))

        tabla_encabezado = [[Paragraph('''<font> <b> </b></font>''', styleSheet["BodyText"])],
                       [Paragraph('''<font> <b> Fecha </b></font>''', estilo_tabla_encabezado),
                        Paragraph('''<font> <b> Descripción </b></font>''', estilo_tabla_encabezado)]]

        tabla = self.obj_hist_clin.tbl_lista_hist_clin
        cantidad_filas = tabla.rowCount()

        for item in range(cantidad_filas):
            #fecha
            f = tabla.item(item, 0).text()  # item es el número de fila; 0 es la columna
            #descripcion
            d = tabla.item(item, 1).text()

            fecha = " <font>" + str(f) + "</font>"
            descripcion = " <font>" + str(d) + "</font>"

            tabla_encabezado.append([Paragraph(fecha, estilo_texto),
                                Paragraph(descripcion, estilo_texto)])

            t_lista_his_clin = Table(tabla_encabezado, (65, 500))
            t_lista_his_clin.setStyle(TableStyle([
                ('INNERGRID', (0, 1), (-1, -1), 0.25, colors.black), #bordes de las celdas
                ('BOX', (0, 1), (-1, -1), 0.25, colors.black), #bordes de la tabla
                ('BACKGROUND', (0, 1), (-1, 1), colors.white) #fondo
            ]))

        documento_pdf.append(t_lista_his_clin)

        #Define dónde se guardará el archivo
        ruta_raiz = os.path.dirname(os.path.abspath(__file__)) + "/pdf"

        #Si esa ruta no existe, la crea
        if not os.path.exists(ruta_raiz):
            os.makedirs(ruta_raiz)

        doc = SimpleDocTemplate(ruta_raiz + "/historia_clinica_" + self.nombre_paciente + "_" + str(datetime.date.today()) + ".pdf",
                                pagesize=A4, rightMargin=14, leftMargin=14, topMargin=5, bottomMargin=18)

        #Cuando hago el build, también le paso como parámetro la clase que construye el pie de página
        doc.build(documento_pdf, canvasmaker=FooterCanvas)

        #Esto es para abrir el PDF apenas se haya terminado de guardar
        if sys.platform == 'linux':
            subprocess.call(["xdg-open", ruta_raiz + "/historia_clinica_" + self.nombre_paciente + "_" + str(datetime.date.today()) + ".pdf"])
        else:
            os.startfile(ruta_raiz + "/historia_clinica_" + self.nombre_paciente + "_" + str(datetime.date.today()) + ".pdf")
