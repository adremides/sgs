from PyQt5.QtWidgets import QWidget, QMessageBox, QTableWidgetItem, QDesktopWidget
from PyQt5 import QtCore, QtGui
from pacientes import Ui_pacientes
from w_form_pacientes import FormPacientes
from w_historias_clinicas import VentanaHistoriasClinicas
from base_datos import Base
from ConfigGlobal import Ventana


class VentanaPacientes(QWidget):
    obj_pacientes = Ui_pacientes()
    id_paciente = -1
    lista_pacientes = []
    pacientes = None

    def __init__(self, permisos):
        QWidget.__init__(self)
        self.obj_pacientes.setupUi(self)

        Ventana(self)

        self.obj_pacientes.tbl_lista_pacientes.setColumnHidden(5, True)

        self.cargar_lista_pacientes()
        # a mí no me aparce centrado en la pantalla, así que lo centro así
        self.centrar_widget()
        self.obj_pacientes.btn_agregar_paciente.clicked.connect(self.agregar)
        self.obj_pacientes.tbl_lista_pacientes.cellClicked.connect(self.seleccion_id_paciente)
        self.obj_pacientes.btn_modificar_paciente.clicked.connect(self.modificar)
        self.obj_pacientes.btn_ver_historia_clinica.clicked.connect(self.ventana_hist_clinicas)
        # Me fijo si el usuario tiene permisos para ver/crear historias clinicas
        if permisos.find("ver_his_clin") != -1:
            self.obj_pacientes.btn_ver_historia_clinica.setVisible(True)
        else:
            self.obj_pacientes.btn_ver_historia_clinica.setVisible(False)
        self.obj_pacientes.tbl_lista_pacientes.itemSelectionChanged.connect(self.habilitar_btn_modificar_y_ver_hist)
        # la variable "action" (en este caso "agregar") se usa para diferenciar si se presionó
        # el botón "Agregar paciente" o el de "modificar paciente"
        # Declaro acá la instancia de w_form_pacientes
        self.pacientes = FormPacientes("A", obj_paciente=None, obj_enfermedades=None)
        # Para poder captar la señal enviada apenas se cierre la ventana de agregar paciente
        # y así poder recargar la grilla ni bien haya terminado de hacer el insert en tabla pacientes
        self.pacientes.senial.connect(self.recargar_lista_pacientes)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def cargar_lista_pacientes(self):
        while (self.obj_pacientes.tbl_lista_pacientes.rowCount() > 0):
            self.obj_pacientes.tbl_lista_pacientes.removeRow(0)
        del self.lista_pacientes[:]
        self.lista_pacientes = Base.Paciente.get_pacientes() # busco los pacientes en la base de datos
        # Cargo la grilla de Pacientes
        if self.lista_pacientes != False:
            for item in self.lista_pacientes:
                rowPosition = self.obj_pacientes.tbl_lista_pacientes.rowCount()
                self.obj_pacientes.tbl_lista_pacientes.insertRow(rowPosition)

                direccion = item.direccion_calle + ', Nº ' + str(item.direccion_numero)
                if item.direccion_piso != None:
                    direccion = direccion + ', piso ' + str(item.direccion_piso)
                if item.direccion_dpto != "":
                    direccion = direccion + ', dpto. ' + str(item.direccion_dpto)

                self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 0, QTableWidgetItem(item.apellido))
                self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 1, QTableWidgetItem(item.nombre))
                if item.activo == True:
                    self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 2, QTableWidgetItem("Activo"))
                else:
                    self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 2, QTableWidgetItem(""))
                if item.telefono_fijo != None:
                    self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 3, QTableWidgetItem(str(item.telefono_fijo)))
                else:
                    self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 3, QTableWidgetItem(""))
                self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 4, QTableWidgetItem(direccion))
                # esto el usuario no lo ve
                self.obj_pacientes.tbl_lista_pacientes.setItem(rowPosition, 5, QTableWidgetItem(str(item.id_paciente)))
        # Ordeno lo que aparece en la tabla por Apellido. es un sorting por columna
        self.obj_pacientes.tbl_lista_pacientes.sortItems(0, QtCore.Qt.AscendingOrder)

    def centrar_widget(self):
        # qr = tamaño del QWidget
        qr = self.frameGeometry()
        # clase QDesktopWidget tiene información acerca del tamaño de la pantalla del usuario.
        # Así, puede determinar dónde está el centro y dónde poner el QWidget para que quede centrado en pantalla
        # cp = tamaño de la pantalla
        cp = QDesktopWidget().availableGeometry().center()
        # Mueve el centro de qr al centro de cp
        qr.moveCenter(cp)
        # mueve el widget arriba a la izquierda de su punto central, así esa ventana queda centrada en pantalla
        self.move(qr.topLeft())

    # Los botones para modificar paciente y para ver historia clínica de un paciente
    # no deberían estar habilitados, a menos que haya un elemento de la tabla seleccionado
    def habilitar_btn_modificar_y_ver_hist(self):
        if self.obj_pacientes.tbl_lista_pacientes.selectedItems():
            self.obj_pacientes.btn_modificar_paciente.setEnabled(True)
            self.obj_pacientes.btn_ver_historia_clinica.setEnabled(True)
        else:
            self.obj_pacientes.btn_modificar_paciente.setEnabled(False)
            self.obj_pacientes.btn_ver_historia_clinica.setEnabled(False)

    # La columna con el ID del paciente está oculta, pero igual se puede obtener el dato
    def seleccion_id_paciente(self, clickedIndex):
        id_paciente = self.obj_pacientes.tbl_lista_pacientes.item(clickedIndex, 5) # id_paciente es de clase QTableWidgetItem
        # por eso acá obtenemos el texto con text() y lo convertimos en int() para que pueda usarse
        # en las consultas a la base de datos, que en general son búsquedas con filtros para traer
        # algunos registros de la base en lugar de todos los registros
        self.id_paciente = int(id_paciente.text())

    def agregar(self):
        # la variable "action" (en este caso "agregar") se usa para diferenciar si se presionó
        # el botón "Agregar paciente" o el de "modificar paciente",
        # y se modifica la ventana acorde a alguna de esas dos acciones
        #self.pacientes = FormPacientes("A", obj_paciente=None, obj_enfermedades=None)
        self.pacientes.show()            # estos dos objetos los envío desde acá vacíos en lugar de
                                         # inicializarlos vacíos en w_form_pacientes, porque
                                         # cuando quiera modificar yo necesito que esa variable en w_form_pacientes
                                         # no esté inicializada, sino que pueda recibir un objeto cargado de datos
    def modificar(self):
        try:
            # Hago la búsqueda del paciente y en la tabla enfermedades. Si falla alguna consulta, salta
            # una excepción y no se abrirá la ventana, sólo se verá un dialog que diga que hubo un error
            obj_paciente = Base.Paciente.get_datos_para_modificar(self.id_paciente)
            obj_enfermedades = Base.Enfermedad.get_lista_enfermedades(self.id_paciente)
            # variable "action" para "modificar"; envío obj_paciente con los datos del paciente, y
            # obj_enfermedades con una lista de enfermedades, para traer todos esos datos
            # precargados en la ventana de modificar paciente
            self.pacientes = FormPacientes("M", obj_paciente, obj_enfermedades)
            self.pacientes.show()
        except Exception as e:
            print(e)
            QMessageBox.warning(self, "Advertencia", "No se ha encontrado el paciente.")

    def ventana_hist_clinicas(self):
        try:
            lista_hist_clinica = Base.Historia_clinica.get_lista_hist_clin(self.id_paciente)
            self.hist_clin = VentanaHistoriasClinicas(lista_hist_clinica, self.id_paciente)
            self.hist_clin.show()
        except:
            QMessageBox.warning(self, "Advertencia", "No se ha encontrado historia clínica.")

    def recargar_lista_pacientes(self):
        del self.lista_pacientes[:]
        self.lista_pacientes = Base.Paciente.get_pacientes()
        self.cargar_lista_pacientes()
