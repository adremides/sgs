import sys, os, subprocess, datetime
from datetime import date
from PyQt5.QtWidgets import QWidget, QDesktopWidget, QTableWidgetItem
from PyQt5.QtCore import QStringListModel
from PyQt5 import QtCore, QtGui, QtWidgets
from turnos import Ui_turnos
from base_datos import Base
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.pagesizes import A4
from reportlab.lib.enums import TA_LEFT, TA_CENTER
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from combobox_con_filtro import ExtendedComboBox
from ConfigGlobal import Ventana

# esta clase define el formato del pie de página del PDF generado con reportlab
# By Juan Fco. Roco at https://stackoverflow.com/a/28283732/10255191

class FooterCanvas(canvas.Canvas):
    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        self.pages = []

    def showPage(self):
        self.pages.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        page_count = len(self.pages)
        for page in self.pages:
            self.__dict__.update(page)
            self.draw_canvas(page_count)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_canvas(self, page_count):
        page = "Página %s de %s" % (self._pageNumber, page_count)
        x = 128
        self.saveState()
        self.setStrokeColorRGB(0, 0, 0)  # Color del borde de la línea
        self.setLineWidth(0.5)
        self.line(66, 78, A4[0] - 66, 78)
        #pdfmetrics.registerFont(TTFont('Arial', 'Arial.ttf'))
        #self.setFont('Arial', 9)
        self.setFont('Times-Roman', 10)
        self.drawString(A4[0]-x, 65, page)
        self.restoreState()


class VentanaTurnos(QWidget):
    obj_turnos = Ui_turnos()
    lista_turnos = []
    lista_horarios = ['08:00', '08:30', '09:00', '09:30', '10:00', '10:30', '11:00', '11:30', '12:00', '12:30',
                      '16:00', '16:30', '17:00']
    contador = -1
    fecha_pdf = ""
    paciente_pdf = ""

    def __init__(self):
        QWidget.__init__(self)
        self.obj_turnos.setupUi(self)

        Ventana(self)

        # a mí no me aparce centrado en la pantalla, así que lo centro así
        self.centrar_widget()
        self.cargar_lista_turnos()
        self.obj_turnos.tbl_turnos.verticalHeader().setVisible(False)
        # Van también las modificaciones en los controles
        self.obj_turnos.tbl_turnos.setColumnHidden(4, True)  # Ocultar columna de id_paciente
        self.obj_turnos.btn_ver_reporte_turnos.clicked.connect(self.imprimir_reporte)
        self.obj_turnos.cal_turnos.selectionChanged.connect(self.cargar_turnos_dia_seleccionado)

    def cargar_lista_turnos(self):
        while self.obj_turnos.tbl_turnos.rowCount() > 0:
            self.obj_turnos.tbl_turnos.removeRow(0)

        # Cargo la grilla de turnos, van las cosas vacías excepto la hora
        for item in self.lista_horarios:
            rowPosition = self.obj_turnos.tbl_turnos.rowCount()
            self.obj_turnos.tbl_turnos.insertRow(rowPosition)
            combo = ExtendedComboBox()
            combo.setModel(QStringListModel(Base.Paciente.get_lista_pacientes_nombres_y_apellidos()))
            self.contador = int(combo.count())
            combo.currentIndexChanged.connect(self.cambio)
            self.obj_turnos.tbl_turnos.setItem(rowPosition, 0, QTableWidgetItem(item))  # Hora
            self.obj_turnos.tbl_turnos.setCellWidget(rowPosition, 1, combo)  # Paciente
            self.obj_turnos.tbl_turnos.setItem(rowPosition, 2, QTableWidgetItem(""))  # Estado
            self.obj_turnos.tbl_turnos.setItem(rowPosition, 3, QTableWidgetItem(""))  # Motivo de la consulta
        self.obj_turnos.tbl_turnos.resizeColumnsToContents()

    def cambio(self):
        combo = self.sender()
        self.eliminar_item_de_mas()
        celda_combo = self.obj_turnos.tbl_turnos.indexAt(combo.pos())
        self.paciente_pdf = self.obj_turnos.tbl_turnos.cellWidget(celda_combo.row(), celda_combo.column()).currentText()
        print("El paciente que hay en la celda actual ("+ str(celda_combo.column()) + ", "
              + str(celda_combo.row()) + ") es " + self.paciente_pdf)

    # Por defecto, cuando un combobox es editable (y este necesita serlo porque si no, no se puede aplicar lo del filtro),
    # se le pueden añadir items a la lista escribiendo algo y luego presionando la tecla Enter. Como no me salió lo de
    # filtrar ese evento, lo que se hace acá es dejar que el usuario ingrese algo nuevo, pero enseguida quitarlo de la
    # lista. (lógica: yo tengo una cantidad de pacientes. Si la cantidad de pacientes en el combobox excede
    # a la cantidad de pacientes que hay en la tabla pacientes, entonces ese item fue
    # agregado por el usuario y hay que eliminarlo)
    # Seguramente no es el mejor camino a seguir, pero habría que averiguar entonces cómo filtrar el evento de la
    # tecla Enter, sin que eso afecte al resto de las teclas del teclado.
    def eliminar_item_de_mas(self):
        combo = self.sender()
        if int(combo.currentIndex() + 1) > self.contador:
            combo.removeItem(combo.currentIndex())

    def cargar_turnos_dia_seleccionado(self):
        del self.lista_turnos[:]
        self.lista_turnos = Base.Turno.get_turnos(str(self.obj_turnos.cal_turnos.selectedDate().toPyDate()))  # busco turnos en la base de datos
        tabla = self.obj_turnos.tbl_turnos
        cantidad_filas = tabla.rowCount()
        i = 0
        if len(self.lista_turnos) > 0:  # Me aseguro de que traje cosas de la base de datos
            for item in range(cantidad_filas):  # Recorro la tabla de turnos
                # El contador i empieza desde 0, pero la cantidad de elementos de la lista empieza a contar desde 1,
                # y voy a recorrer esa lista tantas veces como elementos haya traído desde la base de datos.
                if len(self.lista_turnos) >= (i + 1):
                    # Acá comparo si la hora que hay en la fila actual coincide con la hora que tiene el elemento
                    # trado desde la base de datos
                    if (tabla.item(item, 0).text() == str(self.lista_turnos[i].hora)[:5]):
                        # Si coinciden, significa que para esa hora hay un turno dado, entonces completo
                        # la fila de la grilla con cada dato

                        # Seteo paciente
                        self.obj_turnos.tbl_turnos.cellWidget(item, 1).setCurrentIndex(int(self.lista_turnos[i].id_paciente))
                        # Seteo estado
                        self.obj_turnos.tbl_turnos.setItem(item, 2,
                                                           QTableWidgetItem(self.lista_turnos[i].estado))
                        # Seteo motivo
                        self.obj_turnos.tbl_turnos.setItem(item, 3,
                                                           QTableWidgetItem(self.lista_turnos[i].motivo_consulta))
                        # Si este elemento de la lista coincidió con la fila, entonces paso a comparar el siguiente
                        # elemento de la lista traída desde la base de datos, por eso aumento el contador en 1
                        i = i + 1
                    else:
                        # Si no hay coincidencia, tiene que seguir comparando, así que le pongo que continúe el ciclo for
                        continue

    def centrar_widget(self):
        # qr = tamaño del QWidget
        qr = self.frameGeometry()
        # clase QDesktopWidget tiene información acerca del tamaño de la pantalla del usuario. Así, puede determinar
        # dónde está el centro y dónde poner el QWidget para que quede centrado en pantalla
        # cp = tamaño de la pantalla
        cp = QDesktopWidget().availableGeometry().center()
        # Mueve el centro de qr al centro de cp
        qr.moveCenter(cp)
        # mueve el widget arriba a la izquierda de su punto central, así esa ventana queda centrada en pantalla
        self.move(qr.topLeft())

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    def imprimir_reporte(self):
        documento_pdf = []
        styleSheet = getSampleStyleSheet()

        estilo_texto = ParagraphStyle('', fontSize=10, textColor='#000', rightIndent=0, alignment=TA_LEFT)
        estilo_tabla_encabezado = ParagraphStyle('', fontSize=10, alignment=TA_CENTER, spaceBefore=0,
                                                 spaceAfter=0, textColor='#000', leftIndent=0)
        # creo un estilo para el titulo, le puse Titulo1 porque se lo inventé, se le puede poner otro nombre
        styleSheet.add(ParagraphStyle(name='Titulo1', alignment=TA_CENTER, fontSize=12, textColor='#000'))

        documento_pdf.append(Spacer(0, 50))

        titulo = '<font><b>LISTADO DE TURNOS DEL DÍA '\
                 + str(self.obj_turnos.cal_turnos.selectedDate().toString("dd/MM/yyyy"))\
                 + '</b></font>'

        documento_pdf.append(Paragraph(titulo, styleSheet["Titulo1"]))

        tabla_encabezado = [[Paragraph('''<font> <b> </b></font>''', styleSheet["BodyText"])],
                            [Paragraph('''<font> <b> Hora </b></font>''', estilo_tabla_encabezado),
                             Paragraph('''<font> <b> Paciente </b></font>''', estilo_tabla_encabezado),
                             Paragraph('''<font> <b> Estado </b></font>''', estilo_tabla_encabezado),
                             Paragraph('''<font> <b> Motivo de la consulta </b></font>''', estilo_tabla_encabezado)]]

        tabla = self.obj_turnos.tbl_turnos
        cantidad_filas = tabla.rowCount()

        for item in range(cantidad_filas):
            # Hora
            h= tabla.item(item, 0).text()
            # Paciente (nombre y apellido)
            p = self.obj_turnos.tbl_turnos.cellWidget(item, 1).currentText()
            # Estado
            e = tabla.item(item, 2).text()
            # Motivo de la consulta
            m = tabla.item(item, 3).text()

            hora = " <font>" + str(h) + "</font>"
            paciente = " <font>" + str(p) + "</font>"
            estado = " <font>" + str(e) + "</font>"
            motivo = " <font>" + str(m) + "</font>"

            tabla_encabezado.append([Paragraph(hora, estilo_texto),
                                Paragraph(paciente, estilo_texto),
                                Paragraph(estado, estilo_texto),
                                Paragraph(motivo, estilo_texto)])

            t_lista_turnos = Table(tabla_encabezado, (65, 170, 80, 250))
            t_lista_turnos.setStyle(TableStyle([
                ('INNERGRID', (0, 1), (-1, -1), 0.25, colors.black),  # bordes de las celdas
                ('BOX', (0, 1), (-1, -1), 0.25, colors.black),  # bordes de la tabla
                ('BACKGROUND', (0, 1), (-1, 1), colors.white)  # fondo
            ]))

        documento_pdf.append(t_lista_turnos)

        # Define dónde se guardará el archivo
        ruta_raiz = os.path.dirname(os.path.abspath(__file__)) + "/pdf"

        # Si esa ruta no existe, la crea
        if not os.path.exists(ruta_raiz):
            os.makedirs(ruta_raiz)

        doc = SimpleDocTemplate(ruta_raiz + "/turnos_" + str(datetime.date.today()) + ".pdf",
                                pagesize=A4, rightMargin=14, leftMargin=14, topMargin=5, bottomMargin=18)

        # Cuando hago el build, también le paso como parámetro la clase que construye el pie de página
        doc.build(documento_pdf, canvasmaker=FooterCanvas)

        # Esto es para abrir el PDF apenas se haya terminado de guardar
        if sys.platform == 'linux':
            subprocess.call(["xdg-open", ruta_raiz + "/turnos_" + str(datetime.date.today()) + ".pdf"])
        else:
            os.startfile(ruta_raiz + "/turnos_" + str(datetime.date.today()) + ".pdf")
