from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from passlib.hash import bcrypt
from sqlalchemy import Column, Date, String, Integer, ForeignKey, Boolean, update, delete, Time


class Base(object):
    engine = create_engine('postgresql://postgres:clave@localhost:5432/valcec')
    session = sessionmaker(bind=engine)
    Base = declarative_base()

    def __init__(self):
        self.Base.metadata.create_all(self.engine)

    class Paciente(Base):
        __tablename__ = "pacientes"
        id_paciente = Column(Integer, primary_key=True, autoincrement=True)
        nombre = Column(String)
        apellido = Column(String)
        fecha_nacimiento = Column(Date)
        documento = Column(String, unique=True)
        sexo = Column(String)
        obra_social = Column(String)
        sangre_tipo = Column(String)
        sangre_factor_rh = Column(String)
        activo = Column(Boolean)
        direccion_calle = Column(String)
        direccion_numero = Column(Integer)
        direccion_piso = Column(Integer)
        direccion_dpto = Column(String)
        telefono_fijo = Column(String)
        telefono_movil = Column(String)
        email = Column(String)
        antecedentes_medicos = Column(String)

        def __init__(self, nombre, apellido, fecha_nacimiento, documento, sexo, obra_social, sangre_tipo,
                     sangre_factor_rh, activo, direccion_calle, direccion_numero, direccion_piso, direccion_dpto,
                     telefono_fijo, telefono_movil, email, antecedentes_medicos):
            self.nombre = nombre
            self.apellido = apellido
            self.fecha_nacimiento = fecha_nacimiento
            self.documento = documento
            self.sexo = sexo
            self.obra_social = obra_social
            self.sangre_tipo = sangre_tipo
            self.sangre_factor_rh = sangre_factor_rh
            self.activo = activo
            self.direccion_calle = direccion_calle
            self.direccion_numero = direccion_numero
            self.direccion_piso = direccion_piso
            self.direccion_dpto = direccion_dpto
            self.telefono_fijo = telefono_fijo
            self.telefono_movil = telefono_movil
            self.email = email
            self.antecedentes_medicos = antecedentes_medicos

        @classmethod
        # Hace el select de todos los pacientes, y los manda a la grilla de la ventana Pacientes
        def get_pacientes(self):
            bd = Base()
            sesion = bd.session()
            lista_pacientes = sesion.query(bd.Paciente).all()
            try:
                sesion.close()
                return lista_pacientes
            except Exception:
                print(str(Exception))
                sesion.close()
                return False

        @classmethod
        # Hace select de todos los pacientes, devuelve una lista con nombres y apellidos
        def get_lista_pacientes_nombres_y_apellidos(self):
            bd = Base()
            sesion = bd.session()
            lista_nombres_y_apellidos = []
            try:
                lista_pacientes = sesion.query(bd.Paciente).order_by(bd.Paciente.id_paciente.asc()).all()
                primer_valor = ""
                lista_nombres_y_apellidos.append(primer_valor)
                for item in lista_pacientes:
                    nombre_completo = item.apellido + ", " + item.nombre
                    lista_nombres_y_apellidos.append(nombre_completo)
                sesion.close()
                return lista_nombres_y_apellidos
            except:
                sesion.close()
                return False

        @classmethod
        # Busca un paciente por ID. y devuelve el nombre y el apellido concatenados
        def get_paciente(self, id_paciente):
            bd = Base()
            sesion = bd.session()
            paciente = sesion.query(bd.Paciente).filter(bd.Paciente.id_paciente == id_paciente).one()
            try:
                nombre_apellido = paciente.apellido + ", " + paciente.nombre
                sesion.close()
                return nombre_apellido
            except:
                sesion.close()
                return False

        @classmethod
        # Hace el insert de nuevo paciente
        def guardar_paciente(cls, obj_paciente, lista_enfermedades):
            bd = Base()
            sesion = bd.session()
            paciente = bd.Paciente(obj_paciente.nombre, obj_paciente.apellido, obj_paciente.fecha_nacimiento,
                                   obj_paciente.documento, obj_paciente.sexo, obj_paciente.obra_social,
                                   obj_paciente.sangre_tipo,
                                   obj_paciente.sangre_factor_rh, obj_paciente.activo, obj_paciente.direccion_calle,
                                   obj_paciente.direccion_numero, obj_paciente.direccion_piso,
                                   obj_paciente.direccion_dpto,
                                   obj_paciente.telefono_fijo, obj_paciente.telefono_movil, obj_paciente.email,
                                   obj_paciente.antecedentes_medicos)

            if lista_enfermedades != False:  # si la lista está vacía, no debería intentar agregar nada a la tabla Enfermedades
                paciente.enfermedades = lista_enfermedades
            sesion.add(paciente)
            try:
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

        @classmethod
        # Busca un paciente por ID para llevar sus datos a la pantalla de "Modificar paciente"
        def get_datos_para_modificar(self, id):
            bd = Base()
            sesion = bd.session()
            try:
                paciente = sesion.query(bd.Paciente).filter(bd.Paciente.id_paciente == id).one()
                sesion.close()
                return paciente
            except:
                sesion.close()
                return False

        @classmethod
        # Hace el update de paciente
        def actualizar_paciente(cls, obj_paciente, id_paciente):
            bd = Base()
            sesion = bd.session()
            query = update(bd.Paciente).where(bd.Paciente.id_paciente == id_paciente).values(
                        nombre=obj_paciente.nombre, apellido=obj_paciente.apellido,
                        fecha_nacimiento=obj_paciente.fecha_nacimiento,
                        sexo=obj_paciente.sexo, obra_social=obj_paciente.obra_social,
                        sangre_tipo=obj_paciente.sangre_tipo, sangre_factor_rh=obj_paciente.sangre_factor_rh,
                        activo=obj_paciente.activo, direccion_calle=obj_paciente.direccion_calle,
                        direccion_numero=obj_paciente.direccion_numero, direccion_piso=obj_paciente.direccion_piso,
                        direccion_dpto=obj_paciente.direccion_dpto, telefono_fijo=obj_paciente.telefono_fijo,
                        telefono_movil=obj_paciente.telefono_movil, email=obj_paciente.email,
                        antecedentes_medicos=obj_paciente.antecedentes_medicos)
            try:
                sesion.execute(query)
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

    class Enfermedad(Base):
        __tablename__ = "enfermedades"
        id_enfermedad = Column(Integer, primary_key=True, autoincrement=True)
        fecha = Column(Date)
        enfermedad = Column(String)
        id_paciente = Column(Integer, ForeignKey('pacientes.id_paciente'))
        paciente = relationship("Paciente", backref=backref("enfermedades"))

        def __init__(self, fecha, enfermedad, id_paciente=None):
            self.fecha = fecha
            self.enfermedad = enfermedad
            self.id_paciente = id_paciente

        @classmethod
        # Hace el select de todas las enfermedades relacionadas a un paciente, y las manda a la grilla de la ventana Modificar Paciente
        def get_lista_enfermedades(self, id_paciente):
            bd = Base()
            sesion = bd.session()
            try:
                lista_enfermedades = sesion.query(bd.Enfermedad).filter(bd.Enfermedad.id_paciente == id_paciente).order_by(bd.Enfermedad.fecha.desc()).all()
                sesion.close()
                return lista_enfermedades
            except:
                sesion.close()
                return False

        @classmethod
        # Hace un insert de cada elemento de una lista de objetos de clase Enfermedad
        # Esto viene cuando se está modificando un paciente y se le agregan nuevas enfermedades
        def guardar_enfermedades(cls, lista_enfermedades):
            bd = Base()
            sesion = bd.session()
            for item in lista_enfermedades:
                enfermedad = bd.Enfermedad(item.fecha, item.enfermedad, item.id_paciente)
                sesion.add(enfermedad)
            try:
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

        @classmethod
        # Update de enfermedades; cuando se modifica un paciente
        def modificar_enfermedades(cls, lista_enfermedades):
            bd = Base()
            sesion = bd.session()
            for item in lista_enfermedades:
                query = update(bd.Enfermedad).where(bd.Enfermedad.id_enfermedad == item.id_enfermedad).values(
                                fecha=item.fecha, enfermedad=item.enfermedad)
                sesion.execute(query)
            try:
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

        @classmethod
        # Delete de enfermedades; cuando se modifica un paciente
        def eliminar_enfermedades(cls, lista_ids_a_borrar):
            bd = Base()
            sesion = bd.session()
            lista_ids_a_borrar.sort()
            for id in lista_ids_a_borrar:
                query = delete(bd.Enfermedad).where(bd.Enfermedad.id_enfermedad == id)
                sesion.execute(query)
            try:
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

    class Turno(Base):
        __tablename__ = "turnos"
        id_turno = Column(Integer, primary_key=True, autoincrement=True)
        fecha = Column(Date)
        motivo_consulta = Column(String)
        hora = Column(Time)
        estado = Column(String)
        id_paciente = Column(Integer, ForeignKey('pacientes.id_paciente'))
        paciente = relationship("Paciente", backref=backref("turnos", uselist=False))

        def __init__(self, fecha, id_paciente, motivo_consulta, estado, hora):
            self.fecha = fecha
            self.id_paciente = id_paciente
            self.motivo_consulta = motivo_consulta
            self.estado = estado
            self.hora = hora

        @classmethod
        # Hace el select de todos los turnos, y los manda a la grilla de la ventana turnos
        def get_turnos(self, fecha):
            bd = Base()
            sesion = bd.session()
            lista_turnos = sesion.query(bd.Turno).filter(bd.Turno.fecha == fecha).order_by(bd.Turno.hora.asc()).all()
            try:
                sesion.close()
                return lista_turnos
            except:
                sesion.close()
                return False

    class Historia_clinica(Base):
        __tablename__ = "historias_clinicas"
        id_historia_clinica = Column(Integer, primary_key=True, autoincrement=True)
        fecha = Column(Date)
        descripcion = Column(String)
        id_paciente = Column(Integer, ForeignKey('pacientes.id_paciente'))
        paciente = relationship("Paciente", backref=backref("historias_clinicas"))

        def __init__(self, fecha, descripcion, id_paciente):
            self.fecha = fecha
            self.descripcion = descripcion
            self.id_paciente = id_paciente

        @classmethod
        # Busca historia clínica por ID del paciente para llevar esos datos a la pantalla de Ver historias clínicas
        def get_lista_hist_clin(self, id_paciente):
            bd = Base()
            sesion = bd.session()
            lista_hist_clin = sesion.query(bd.Historia_clinica).filter(bd.Historia_clinica.id_paciente == id_paciente).order_by(bd.Historia_clinica.fecha.desc()).all()
            try:
                sesion.close()
                return lista_hist_clin
            except:
                sesion.close()
                return False

        @classmethod
        # Hace insert de una nueva entrada al historial clínico de un paciente
        def guardar_hist_clin(cls, obj_hist_clin):
            bd = Base()
            sesion = bd.session()
            hist_clin = bd.Historia_clinica(obj_hist_clin.fecha, obj_hist_clin.descripcion, obj_hist_clin.id_paciente)
            sesion.add(hist_clin)
            try:
                sesion.commit()
                sesion.close()
                return True
            except:
                sesion.rollback()
                sesion.close()
                return False

    class Usuario(Base):
        __tablename__ = "usuarios"
        id_usuario = Column(Integer, primary_key=True, autoincrement=True)
        nombre_usuario = Column(String, unique=True)
        clave = Column(String)
        permisos = Column(String)

        def __init__(self, nombre_usuario, clave, permisos):
            self.nombre_usuario = nombre_usuario
            self.clave = clave
            self.permisos = permisos

        def __repr__(self):
            return self.nombre_usuario

        @classmethod
        # este es el método que hace la validación del usuario al iniciar sesión
        def get_usuario(self, nombre_usuario, clave):
            bd = Base()
            sesion = bd.session()
            try:
                # si puse mal el nombre_usuario, la consulta falla y salta una excepción
                usuario = sesion.query(bd.Usuario).filter(bd.Usuario.nombre_usuario == nombre_usuario).one()
                sesion.close()

                if bcrypt.verify(clave, usuario.clave): #acá verifica la contraseña
                    return usuario  # si los datos coinciden con lo que está almacenado en la base de datos,
                                    # entonces devuelvo un objeto con los datos del usuario
                else:
                    return False  # si puse mal la contraseña, el objeto usuario
                                  # será de tipo None y por eso devuelvo False
            except:
                sesion.close()
                return False

        @classmethod
        # Hace select de todos los usuarios, devuelve una lista con nombres
        def get_lista_usuarios_nombres(self):
            bd = Base()
            sesion = bd.session()
            lista_usuarios_nombres = []
            try:
                lista_usuarios = sesion.query(bd.Usuario).order_by(bd.Usuario.id_usuario.asc()).all()
                for item in lista_usuarios:
                    nombre = item.nombre_usuario
                    lista_usuarios_nombres.append(nombre)
                sesion.close()
                return lista_usuarios_nombres
            except:
                sesion.close()
                return False