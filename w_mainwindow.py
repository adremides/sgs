import sys, os
from PyQt5.QtWidgets import QMainWindow, QApplication, QMenu
from PyQt5 import QtCore, QtWidgets # QtWidgets está temporalmente porque tengo el UI desactualizado
from mainwindow import Ui_MainWindow
from w_pacientes import VentanaPacientes
from w_turnos import VentanaTurnos
from base_datos import Base
from ConfigGlobal import Ventana

class VentanaPrincipal(QMainWindow):
    obj_main = Ui_MainWindow()
    permisos = ""
    con_acceso = False

    def __init__(self):
        QMainWindow.__init__(self)
        self.obj_main.setupUi(self)

        Ventana(self)

        self.obj_main.btn_mostrar_login.clicked.connect(self.mostrar_form_login)
        self.obj_main.btn_mostrar_login_logo.clicked.connect(self.mostrar_form_login)
        self.obj_main.btn_iniciar_sesion.clicked.connect(self.iniciar_sesion)
        self.obj_main.btn_pacientes.clicked.connect(self.ventana_pacientes)
        self.obj_main.btn_logout.clicked.connect(self.cerrar_sesion)
        self.obj_main.btn_logout_logo.clicked.connect(self.cerrar_sesion)
        self.obj_main.btn_turnos.clicked.connect(self.ventana_turnos)
        self.obj_main.btn_reportes.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.obj_main.btn_reportes.customContextMenuRequested.connect(self.mostrar_menu_reportes)

        self.obj_main.cbx_usuario.setModel(QtCore.QStringListModel(Base.Usuario.get_lista_usuarios_nombres()))

        self.obj_main.frm_botonera.hide()
        self.obj_main.frm_logout.hide()
        self.obj_main.frm_login_form.hide()
        self.obj_main.lbl_error.setVisible(False)

        # Puse esto así para poder hacer las pruebas y no tener que andar
        # iniciando sesión cada vez que se abre la aplicación
        #self.obj_main.frm_botonera.show()                 ##
        #self.obj_main.frm_login.hide()                    ##
        #self.obj_main.frm_login_form.hide()               ##
        #self.obj_main.lbl_logo_VALCEC.setVisible(False)   ##
        ############## Borrar, luego #######################

    def iniciar_sesion(self):
        nombre_usuario = self.obj_main.cbx_usuario.currentText()
        clave = self.obj_main.lne_clave.text()
        try:
            # si puse mal la contraseña o el usuario, obj_usuario vendrá con valor False
            obj_usuario = Base.Usuario.get_usuario(nombre_usuario, clave)
            if obj_usuario != False:
                self.obj_main.lne_clave.setText("")
                self.permisos = obj_usuario.permisos
                self.obj_main.lbl_logo_VALCEC.setVisible(False)
                self.obj_main.frm_login_form.hide()
                self.obj_main.frm_logout.show()
                self.obj_main.lbl_nombre_usuario.setText(obj_usuario.nombre_usuario)
                self.obj_main.lbl_nombre_usuario.setVisible(True)
                self.obj_main.frm_botonera.show()
                self.obj_main.lbl_error.setVisible(False)
                # como inicié sesión, le cambio el título a la ventana
                self.setWindowTitle("Gestión de Salud   |   Ventana principal")
                self.con_acceso = True
            # cuando el usuario o la contraseña están mal escritos, se limpian los QLineEdit y se muestra el cartelito de error
            else:
                self.con_acceso = False
                self.obj_main.lbl_error.setVisible(True)
                self.obj_main.lne_clave.setText("")
        except:  # si ocurre un error, que dios nos asista (?    :P
            print("Error.")

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_F1:
            if self.con_acceso:
                print("debería abrirse el contenido de Ayuda")
        elif e.key() == QtCore.Qt.Key_F2:
            if self.con_acceso:
                self.ventana_pacientes()
        elif e.key() == QtCore.Qt.Key_F3:
            if self.con_acceso:
                self.ventana_turnos()
        elif e.key() == QtCore.Qt.Key_F4:
            if self.con_acceso:
                print("debería abrirse la ventana de Reportes")
        elif e.key() == QtCore.Qt.Key_Return:
            if self.obj_main.frm_login_form.isVisible():
                self.iniciar_sesion()

    def ventana_pacientes(self):
        # envío string con permisos que tiene el usuario
        self.pacientes = VentanaPacientes(self.permisos)
        #y por eso puedo mostrar u ocultar el botoncito de "Ver historias clínicas" que está dentro de la ventana de "Pacientes"
        self.pacientes.show()

    def ventana_turnos(self):
        self.turnos = VentanaTurnos()
        self.turnos.show()

    def mostrar_form_login(self):
        self.obj_main.frm_login.hide()
        self.obj_main.frm_login_form.show()
        self.obj_main.cbx_usuario.setFocus()

    def cerrar_sesion(self):
        self.obj_main.frm_botonera.hide()
        self.obj_main.frm_login.show()
        self.obj_main.frm_logout.hide()
        self.obj_main.lbl_logo_VALCEC.setVisible(True)

    def mostrar_menu_reportes(self, point):
        menu = QMenu()
        action = menu.addAction("Ver reporte de Turnos")
        action.triggered.connect(self.ventana_reporte_turnos)
        if self.permisos.find("ver_his_clin") != -1:
            menu.addSeparator()
            action = menu.addAction("Ver reporte de Historias clínicas")
            action.triggered.connect(self.ventana_reporte_hist_clin)
        menu.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                           "selection-color: rgb(255, 255, 255);\n"
                           "color: rgb(0, 0, 0);\n"
                           "selection-background-color: rgb(51, 153, 255);\n"
                           "font: 13pt \"MS Shell Dlg 2\";")
        menu.exec_(self.obj_main.btn_reportes.mapToGlobal(point))

    def ventana_reporte_turnos(self):
        self.t = VentanaTurnos()
        self.t.imprimir_reporte()

    def ventana_reporte_hist_clin(self):
        print("show Ventana de Reporte de Historia Clínica")


app = QApplication(sys.argv)

#Esto es para que los menús contextuales de los campos de texto ("cortar", "copiar", "pegar", etc) se vean en español.
#Por alguna razón, con el único control que no funciona es con el QTextEdit
translator = QtCore.QTranslator()
#Copié ese archivito del directorio translate, que estaba dentro de el directorio donde se instaló Qt
translator.load(os.path.dirname(os.path.abspath(__file__)) + "/resources/qt_es.qm")
app.installTranslator(translator)

window = VentanaPrincipal()
window.show()
app.exec_()
