import re
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtWidgets import QWidget, QTableWidgetItem, QMenu, QMessageBox
from form_pacientes_alta_modificacion import Ui_form_pacientes_am
from base_datos import Base
from PyQt5.QtCore import QPersistentModelIndex
from ConfigGlobal import Ventana

class FormPacientes(QWidget):
    obj_form = Ui_form_pacientes_am()
    lista_id_enfermedades_borrar = [] #Guardo IDs, que luego serán enviados al modelo para hacer el delete en la db
    lista_enfermedades_modificadas = [] #Guardo objetos que ya existian de clase Enfermedad, para hacer el update en la db
    lista_enfermedades = [] #Guardo objetos nuevos de clase Enfermedad, para hacer un insert en la db
    id_enfermedad = -1
    id_paciente = -1
    action = ""

    # Indice de los campos de la tabla Enfermedades en el QTableWidget
    campo_fecha = 0
    campo_enfermedad = 1
    campo_id_paciente = 2
    campo_id_enfermedad = 3

    # Formato para mostrar fecha
    formato_mostrar_fecha = 'dd/MM/yyyy'

    # Señal para enviar a w_pacientes y hacer que se recargue la grilla en cuanto se cree un nuevo paciente
    senial = QtCore.pyqtSignal(int)

    def __init__(self, action, obj_paciente, obj_enfermedades):
        QWidget.__init__(self)
        #self.obj_form.setupUi(self, action)
        self.obj_form.setupUi(self)

        Ventana(self)

        self.action = action
        _translate = QtCore.QCoreApplication.translate

        # Modificaciones en los controles
        self.obj_form.tbl_enfermedades.setColumnHidden(2, True) # ocultar id_paciente
        self.obj_form.tbl_enfermedades.setColumnHidden(3, True) # ocultar id_enfermedad
        self.obj_form.btn_guardar_personales.setText(_translate("form_pacientes_am", "Guardar"))
        fecha = QtWidgets.QDateEdit()
        fecha.setCalendarPopup(True)
        fecha.setDisplayFormat(self.formato_mostrar_fecha)
        self.obj_form.tbl_enfermedades.setCellWidget(0, 0, fecha)

        # si action es "modificar", entonces traigo los datos precargados en la ventana
        if action == "M":
            self.setWindowTitle(_translate("form_pacientes_am", "Modificar datos del paciente"))
            self.obj_form.btn_guardar_personales.setText(_translate("form_pacientes_am", "Modificar"))
            self.id_paciente = obj_paciente.id_paciente
            ##  TAB Datos personales  ##
            self.obj_form.lne_nombre.setText(obj_paciente.nombre)
            self.obj_form.lne_apellido.setText(obj_paciente.apellido)
            self.obj_form.dte_fecha_nac.setDate(obj_paciente.fecha_nacimiento)
            self.obj_form.lne_dni.setText(str(obj_paciente.documento))
            index_cbx_sexo = int(self.obj_form.cbx_sexo.findText(str(obj_paciente.sexo)))
            self.obj_form.cbx_sexo.setCurrentIndex(index_cbx_sexo)
            self.obj_form.lne_obra_social.setText(str(obj_paciente.obra_social))
            index_cbx_tipo_sangre = int(self.obj_form.cbx_tipo_sangre.findText(str(obj_paciente.sangre_tipo)))
            self.obj_form.cbx_tipo_sangre.setCurrentIndex(index_cbx_tipo_sangre)
            index_cbx_factor_rh = int(self.obj_form.cbx_factor_rh.findText(str(obj_paciente.sangre_factor_rh)))
            self.obj_form.cbx_factor_rh.setCurrentIndex(index_cbx_factor_rh)
            if obj_paciente.activo:
                self.obj_form.chbx_estado_paciente.setChecked(True)
                self.obj_form.chbx_estado_paciente.setText("Activo")
            else:
                self.obj_form.chbx_estado_paciente.setChecked(False)
                self.obj_form.chbx_estado_paciente.setText("")
            ##  TAB Datos de contacto  ##
            self.obj_form.lne_calle.setText(obj_paciente.direccion_calle)
            if obj_paciente.direccion_numero == None:
                self.obj_form.lne_nro_calle.setText("")
            else:
                self.obj_form.lne_nro_calle.setText(str(obj_paciente.direccion_numero))
            if obj_paciente.direccion_piso == None:
                self.obj_form.lne_piso.setText("")
            else:
                self.obj_form.lne_piso.setText(str(obj_paciente.direccion_piso))
            self.obj_form.lne_departamento.setText(obj_paciente.direccion_dpto)
            self.obj_form.lne_fijo.setText(obj_paciente.telefono_fijo)
            self.obj_form.lne_celular.setText(obj_paciente.telefono_movil)
            self.obj_form.lne_email.setText(obj_paciente.email)
            ##  TAB Enfermedades  ##
            try:
                # me aseguro de que el objeto de la consulta tenga datos, ya que si no tiene nada, no voy a recorrerlo
                if obj_enfermedades != False:
                    for item in obj_enfermedades:
                        rowPosition = self.obj_form.tbl_enfermedades.rowCount()
                        self.obj_form.tbl_enfermedades.insertRow(rowPosition)
                        fecha = QtWidgets.QDateEdit()
                        fecha.setCalendarPopup(True)
                        fecha.setDisplayFormat(self.formato_mostrar_fecha)
                        fecha.setDateTime(QtCore.QDateTime(item.fecha))

                        fecha.dateChanged.connect(self.fecha_modificada)

                        enfermedad = QTableWidgetItem(str(item.enfermedad))
                        id_paciente = QTableWidgetItem(str(item.id_paciente))
                        id_enfermedad = QTableWidgetItem(str(item.id_enfermedad))

                        #self.obj_form.tbl_enfermedades.setItem(rowPosition, 0, QTableWidgetItem(str(item.fecha)))
                        self.obj_form.tbl_enfermedades.setCellWidget(rowPosition, self.campo_fecha, fecha)
                        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_enfermedad, enfermedad)
                        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_id_paciente, id_paciente)
                        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_id_enfermedad, id_enfermedad)
            except Exception as e:
                print("Excepción en la consulta a tabla 'enfermedades':")
                print(e)
            ##  TAB Antecedentes médicos y medicación  ##
            self.obj_form.txt_antecedentes_medicos.setHtml(obj_paciente.antecedentes_medicos)

        # Cada vez que se renderiza la ventana, se vacían las listas para que no se superpongan datos,
        # así se evitan excepciones durante las consultas a la base
        del self.lista_id_enfermedades_borrar[:]
        del self.lista_enfermedades_modificadas[:]
        del self.lista_enfermedades[:]

        # las siguientes 2 líneas son para poner el menú contextual del QTextEdit en español
        self.obj_form.txt_antecedentes_medicos.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.obj_form.txt_antecedentes_medicos.customContextMenuRequested.connect(self.generar_menu_en_espaniol)
        self.obj_form.btn_negrita.clicked.connect(self.formato_negrita)
        self.obj_form.btn_cursiva.clicked.connect(self.formato_cursiva)
        self.obj_form.btn_subrayado.clicked.connect(self.formato_subrayado)
        self.obj_form.btn_guardar_personales.clicked.connect(self.guardar_datos)
        self.obj_form.btn_agregar_fila.clicked.connect(self.agregar_fila)
        self.obj_form.btn_quitar_fila.clicked.connect(self.quitar_fila)
        # se cambia leyenda del checkbox dependiendo de si está tildado o no
        self.obj_form.chbx_estado_paciente.stateChanged.connect(self.cambiar_checkbox_leyenda)
        self.obj_form.tbl_enfermedades.cellClicked.connect(self.seleccion_id_enfermedad)
        # me aseguro de que en el siguiente text edit haya sólo números
        self.obj_form.lne_dni.editingFinished.connect(self.validar_dni)
        self.obj_form.tbl_enfermedades.itemChanged.connect(self.enfermedad_modificada)

    def guardar_datos(self):
        try:
            obj_paciente = Base.Paciente
            obj_paciente.nombre = self.obj_form.lne_nombre.text()
            obj_paciente.apellido = self.obj_form.lne_apellido.text()
            obj_paciente.fecha_nacimiento = self.obj_form.dte_fecha_nac.text()
            obj_paciente.documento = self.obj_form.lne_dni.text()
            if not re.match("^[0-9]*$", obj_paciente.documento):
                QMessageBox.warning(self, "Advertencia", "Por favor, ingrese solo números en el campo DNI.")
                raise Exception
            obj_paciente.sexo = self.obj_form.cbx_sexo.currentText()
            obj_paciente.obra_social = self.obj_form.lne_obra_social.text()
            obj_paciente.sangre_tipo = self.obj_form.cbx_tipo_sangre.currentText()
            obj_paciente.sangre_factor_rh = self.obj_form.cbx_factor_rh.currentText()
            obj_paciente.activo = self.obj_form.chbx_estado_paciente.isChecked()
            obj_paciente.direccion_calle = self.obj_form.lne_calle.text()

            if self.obj_form.lne_nro_calle.text() == "":
                obj_paciente.direccion_numero = None
            else:
                obj_paciente.direccion_numero = int(self.obj_form.lne_nro_calle.text())

            if self.obj_form.lne_piso.text() == "":
                obj_paciente.direccion_piso = None
            else:
                obj_paciente.direccion_piso = int(self.obj_form.lne_piso.text())

            obj_paciente.direccion_dpto = self.obj_form.lne_departamento.text()
            obj_paciente.telefono_fijo = self.obj_form.lne_fijo.text()
            obj_paciente.telefono_movil = self.obj_form.lne_celular.text()
            obj_paciente.email = self.obj_form.lne_email.text()
            obj_paciente.antecedentes_medicos = self.obj_form.txt_antecedentes_medicos.toHtml()

            tabla = self.obj_form.tbl_enfermedades
            cantidad_filas = tabla.rowCount()

            i = 0
            for item in range(cantidad_filas):
                fecha = tabla.cellWidget(item, self.campo_fecha).date().toPyDate()
                enfermedad = tabla.item(item, self.campo_enfermedad).text()
                id_paciente = tabla.item(item, self.campo_id_paciente).text()
                id_enfermedad = tabla.item(item, self.campo_id_enfermedad).text()
                if tabla.item(item, self.campo_id_enfermedad).text() == "":
                    # el problema estaba acá. Lo que se hacía era declarar una variable de tipo Enfermedad,
                    # que luego se usaba para almacenar cada enfermedad, peeero la cosa es que al usar
                    # la misma instancia, se sobreescribían los datos y por eso se guardaban las cosas repetidas
                    # tantas veces como filas hubiese habido en la tabla
                    obj_enfermedad = Base.Enfermedad(fecha,
                                                     enfermedad,
                                                     self.id_paciente) # esta instancia va cargada con fecha y enfermedad
                    # y acá pongo ese objeto de clase Enfermedad dentro de una lista
                    self.lista_enfermedades.insert(i, obj_enfermedad)
                    i = i + 1

            if self.action == "M": # se hace update a un paciente
                Base.Paciente.actualizar_paciente(obj_paciente, int(self.id_paciente))
                if len(self.lista_enfermedades) > 0:
                    # Escenario insert de enfermedades, pero cuando ya había enfermedades guardadas anteriormente
                    Base.Enfermedad.guardar_enfermedades(self.lista_enfermedades)
                if len(self.lista_enfermedades_modificadas) > 0:
                    # Escenario update de enfermedades
                    Base.Enfermedad.modificar_enfermedades(self.lista_enfermedades_modificadas)
                if self.lista_id_enfermedades_borrar != False:
                    # Escenario delete de enfermedades
                    Base.Enfermedad.eliminar_enfermedades(self.lista_id_enfermedades_borrar)
            else: # se hace insert de nuevo paciente
                # la variable lista_enfermedades ahora es una lista con distintos objetos de clase Enfermedad.
                # Cada objeto tiene cada uno de los valores de cada fila de la tabla

                # Escenario insert de paciente + insert de enfermedades
                Base.Paciente.guardar_paciente(obj_paciente, self.lista_enfermedades)
            self.senial.emit(1)  # Envío la señal a la ventana w_historias_clinicas para que recargue la grilla
            self.close()
        except Exception as e:
            print(e)
            if self.action == "M":
                QMessageBox.critical(self, "Error", "No se modificó el paciente.")
            else:
                QMessageBox.critical(self, "Error", "No se guardó el paciente.")

    def generar_menu_en_espaniol(self, point):
        menu = QMenu()
        action = menu.addAction("Deshacer")
        action.triggered.connect(self.action_deshacer)
        action = menu.addAction("Rehacer")
        action.triggered.connect(self.action_rehacer)
        menu.addSeparator()
        action = menu.addAction("Cortar")
        action.triggered.connect(self.action_cortar)
        action = menu.addAction("Copiar")
        action.triggered.connect(self.action_copiar)
        action = menu.addAction("Pegar")
        action.triggered.connect(self.action_pegar)
        menu.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                           "selection-color: rgb(255, 255, 255);\n"
                           "color: rgb(0, 0, 0);\n"
                           "selection-background-color: rgb(51, 153, 255);\n"
                           "font: 13pt \"MS Shell Dlg 2\";")
        menu.exec_(self.obj_form.txt_antecedentes_medicos.mapToGlobal(point))

    def action_deshacer(self):
        self.obj_form.txt_antecedentes_medicos.addAction(self.obj_form.txt_antecedentes_medicos.undo())

    def action_rehacer(self):
        self.obj_form.txt_antecedentes_medicos.addAction(self.obj_form.txt_antecedentes_medicos.redo())

    def action_cortar(self):
        self.obj_form.txt_antecedentes_medicos.addAction(self.obj_form.txt_antecedentes_medicos.cut())

    def action_copiar(self):
        self.obj_form.txt_antecedentes_medicos.addAction(self.obj_form.txt_antecedentes_medicos.copy())

    def action_pegar(self):
        self.obj_form.txt_antecedentes_medicos.addAction(self.obj_form.txt_antecedentes_medicos.paste())

    def agregar_fila(self):
        self.obj_form.tbl_enfermedades.blockSignals(True)
        rowPosition = self.obj_form.tbl_enfermedades.rowCount()
        self.obj_form.tbl_enfermedades.insertRow(rowPosition)
        fecha = QtWidgets.QDateEdit()
        fecha.setDateTime(QtCore.QDateTime.currentDateTime())
        fecha.setCalendarPopup(True)
        fecha.setDisplayFormat(self.formato_mostrar_fecha)
        self.obj_form.tbl_enfermedades.setCellWidget(rowPosition, self.campo_fecha, fecha)
        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_enfermedad, QTableWidgetItem(""))
        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_id_paciente, QTableWidgetItem(""))
        self.obj_form.tbl_enfermedades.setItem(rowPosition, self.campo_id_enfermedad, QTableWidgetItem(""))
        self.obj_form.tbl_enfermedades.blockSignals(False)

    def quitar_fila(self):
        #Eesto permite eliminar de a varias filas de la vista a la vez, dando al posiblidad de eliminar
        # varios registros de la tabla en la base de datos
        indexes = [QPersistentModelIndex(index) for index in self.obj_form.tbl_enfermedades.selectionModel().selectedRows()]
        for index in indexes:
            index_id_enfermedad = self.obj_form.tbl_enfermedades.model().index(index.row(), 3)
            id_enfermedad = int(self.obj_form.tbl_enfermedades.model().data(index_id_enfermedad))
            # voy guardando cada ID de enfermedad de cada fila que eliminé de la vista
            self.lista_id_enfermedades_borrar.append(id_enfermedad)
            self.obj_form.tbl_enfermedades.removeRow(index.row()) # elimino la fila de la vista

    def enfermedad_modificada(self, item):
        self.guardar_enfermedad_modificada(item.row())

    def fecha_modificada(self):
        ctrl_qdateedit = self.sender()
        celda_qdateedit = self.obj_form.tbl_enfermedades.indexAt(ctrl_qdateedit.pos())
        self.guardar_enfermedad_modificada(celda_qdateedit.row())

    def guardar_enfermedad_modificada(self, fila):
        fecha = self.obj_form.tbl_enfermedades.cellWidget(fila, self.campo_fecha).date().toPyDate()
        enf = self.obj_form.tbl_enfermedades.item(fila, self.campo_enfermedad).text()
        id_paciente = self.obj_form.tbl_enfermedades.item(fila, self.campo_id_paciente).text()
        if fecha != None and enf != "" and id_paciente != "":
            enfermedad = Base.Enfermedad(fecha, enf, int(id_paciente))  # creo un objeto de clase Enfermedad
            enfermedad.id_enfermedad = int(self.obj_form.tbl_enfermedades.item(fila, self.campo_id_enfermedad).text())
            self.lista_enfermedades_modificadas.append(enfermedad)  # pongo ese objeto en una lista

    # La columna con el ID de enfermedad está oculta, pero igual se puede obtener el dato
    def seleccion_id_enfermedad(self, clickedIndex):
        id_enfermedad = self.obj_form.tbl_enfermedades.item(clickedIndex, 3).text()  # id_enfermedad es de clase QTableWidgetItem
        if id_enfermedad != "":
            # por eso acá obtenemos el texto con text() y lo convertimos en int() para que pueda usarse en las
            # consultas a la base de datos, que en general son búsquedas con filtros para traer algunos registros
            # de la base en lugar de todos los registros
            self.id_enfermedad = int(id_enfermedad)

    def formato_negrita(self):
        if self.obj_form.txt_antecedentes_medicos.fontWeight() == QtGui.QFont.Bold:
            self.obj_form.txt_antecedentes_medicos.setFontWeight(QtGui.QFont.Normal)
        else:
            self.obj_form.txt_antecedentes_medicos.setFontWeight(QtGui.QFont.Bold)

    def formato_cursiva(self):
        if self.obj_form.txt_antecedentes_medicos.fontItalic():
            self.obj_form.txt_antecedentes_medicos.setFontItalic(False)
        else:
            self.obj_form.txt_antecedentes_medicos.setFontItalic(True)

    def formato_subrayado(self):
        if self.obj_form.txt_antecedentes_medicos.fontUnderline():
            self.obj_form.txt_antecedentes_medicos.setFontUnderline(False)
        else:
            self.obj_form.txt_antecedentes_medicos.setFontUnderline(True)

    def cambiar_checkbox_leyenda(self):
        if self.obj_form.chbx_estado_paciente.isChecked():
            self.obj_form.chbx_estado_paciente.setText("Activo")
        else:
            self.obj_form.chbx_estado_paciente.setText("")

    def validar_dni(self):
        if not re.match("^[0-9]*$", self.obj_form.lne_dni.text()):
            QMessageBox.warning(self, "Advertencia", "Por favor, ingrese solo números en el campo DNI.")
