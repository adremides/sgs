from base_datos import Base
from passlib.hash import bcrypt
from datetime import datetime

bd = Base()

sesion = bd.session()

administrativo = bd.Usuario('administrativo', bcrypt.hash("admin"), 'cancel_turn')
medico = bd.Usuario('médico', bcrypt.hash("doc"), 'ver_his_clin/agr_his_cli')

paciente1 = bd.Paciente('Test 1', 'Test 1', '1978-10-02', '12345678', 'Femenino','', 'B',
                     '-', True,'Fake Street', 123, None, '',
                     '123456', '','email@me.com', '')
paciente2 = bd.Paciente('Nombre 2', 'Apellido 2', '1970-04-03', '87654321', 'Masculino','2', 'O',
                     '+', True,'Calle Falsa', 123, None, '',
                     '654321', '15402222','mail@to.me', '. . .')

#enfermedad = [bd.Enfermedad(datetime.date.today(), "Resfrío")]
#enfermedad = [bd.Enfermedad(datetime.strptime('10/10/2018', '%d/%m/%Y'), "Resfrío")]

#paciente2.enfermedades = enfermedad

sesion.add(administrativo)
sesion.add(medico)
sesion.add(paciente1)
sesion.add(paciente2)

sesion.commit()
sesion.close()