# Configuración global de ventanas
from datetime import date, datetime
from PyQt5 import QtGui

def Ventana(self):
    icon = QtGui.QIcon()
    # el 19 de octubre es el día mundial de la lucha contra el cáncer de mama
    if date.today().month == 11 and date.today().day == 1:
        icon.addPixmap(QtGui.QPixmap("images/pink-ribbon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
    else:
        icon.addPixmap(QtGui.QPixmap("images/salud.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        pass
    self.setWindowIcon(icon)