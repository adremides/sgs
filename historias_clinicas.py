# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'historias_clinicas.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_historias_clinicas(object):
    def setupUi(self, historias_clinicas):
        historias_clinicas.setObjectName("historias_clinicas")
        historias_clinicas.resize(1355, 930)
        font = QtGui.QFont()
        font.setPointSize(13)
        historias_clinicas.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("images/salud.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        historias_clinicas.setWindowIcon(icon)
        self.verticalLayoutWidget = QtWidgets.QWidget(historias_clinicas)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(30, 30, 1291, 40))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.verticalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(25)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.lne_filtro_fecha = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lne_filtro_fecha.setObjectName("lne_filtro_fecha")
        self.horizontalLayout_2.addWidget(self.lne_filtro_fecha)
        self.lne_filtro_descripcion = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lne_filtro_descripcion.setObjectName("lne_filtro_descripcion")
        self.horizontalLayout_2.addWidget(self.lne_filtro_descripcion)
        self.btn_agregar_hist_clin = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.btn_agregar_hist_clin.setMinimumSize(QtCore.QSize(280, 0))
        self.btn_agregar_hist_clin.setMaximumSize(QtCore.QSize(0, 16777215))
        self.btn_agregar_hist_clin.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";")
        self.btn_agregar_hist_clin.setObjectName("btn_agregar_hist_clin")
        self.horizontalLayout_2.addWidget(self.btn_agregar_hist_clin)
        self.btn_ver_reporte = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.btn_ver_reporte.setMinimumSize(QtCore.QSize(200, 0))
        self.btn_ver_reporte.setMaximumSize(QtCore.QSize(0, 16777215))
        self.btn_ver_reporte.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";")
        self.btn_ver_reporte.setObjectName("btn_ver_reporte")
        self.horizontalLayout_2.addWidget(self.btn_ver_reporte)
        self.horizontalLayout.addLayout(self.horizontalLayout_2)
        self.tbl_lista_hist_clin = QtWidgets.QTableWidget(historias_clinicas)
        self.tbl_lista_hist_clin.setGeometry(QtCore.QRect(30, 100, 1291, 791))
        font = QtGui.QFont()
        font.setFamily("MS Shell Dlg 2")
        font.setPointSize(13)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.tbl_lista_hist_clin.setFont(font)
        self.tbl_lista_hist_clin.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.tbl_lista_hist_clin.setStyleSheet("font: 13pt \"MS Shell Dlg 2\";\n"
"background-color: rgb(255, 255, 255);")
        self.tbl_lista_hist_clin.setAlternatingRowColors(True)
        self.tbl_lista_hist_clin.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tbl_lista_hist_clin.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tbl_lista_hist_clin.setObjectName("tbl_lista_hist_clin")
        self.tbl_lista_hist_clin.setColumnCount(3)
        self.tbl_lista_hist_clin.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tbl_lista_hist_clin.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tbl_lista_hist_clin.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tbl_lista_hist_clin.setHorizontalHeaderItem(2, item)
        self.tbl_lista_hist_clin.horizontalHeader().setDefaultSectionSize(200)
        self.tbl_lista_hist_clin.horizontalHeader().setStretchLastSection(True)
        self.tbl_lista_hist_clin.verticalHeader().setStretchLastSection(False)

        self.retranslateUi(historias_clinicas)
        QtCore.QMetaObject.connectSlotsByName(historias_clinicas)

    def retranslateUi(self, historias_clinicas):
        _translate = QtCore.QCoreApplication.translate
        historias_clinicas.setWindowTitle(_translate("historias_clinicas", "Gestión de Salud   |   Historias clínicas"))
        self.lne_filtro_fecha.setPlaceholderText(_translate("historias_clinicas", "Buscar por fecha"))
        self.lne_filtro_descripcion.setPlaceholderText(_translate("historias_clinicas", "Buscar por descripción"))
        self.btn_agregar_hist_clin.setText(_translate("historias_clinicas", "Agregar nueva"))
        self.btn_ver_reporte.setText(_translate("historias_clinicas", "Reporte"))
        item = self.tbl_lista_hist_clin.horizontalHeaderItem(0)
        item.setText(_translate("historias_clinicas", "Fecha"))
        item = self.tbl_lista_hist_clin.horizontalHeaderItem(1)
        item.setText(_translate("historias_clinicas", "Descripción"))
        item = self.tbl_lista_hist_clin.horizontalHeaderItem(2)
        item.setText(_translate("historias_clinicas", "ID"))

