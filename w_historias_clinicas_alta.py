from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QMenu
from historias_clinicas_alta import Ui_historias_clinicas_alta
from base_datos import Base
from ConfigGlobal import Ventana

class VentanaHistoriasClinicasAlta(QWidget):
    obj_hist_clin = Ui_historias_clinicas_alta()
    # Señal
    senial = QtCore.pyqtSignal(int)

    def __init__(self, id_paciente):
        QWidget.__init__(self)
        self.obj_hist_clin.setupUi(self)

        Ventana(self)

        self.id_paciente = id_paciente

        self.obj_hist_clin.txt_descripcion.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.obj_hist_clin.txt_descripcion.customContextMenuRequested.connect(self.generar_menu_en_espaniol)

    def closeEvent(self, QCloseEvent):
        fecha = self.obj_hist_clin.dte_fecha.text()
        descripcion = self.obj_hist_clin.txt_descripcion.toPlainText()
        id_paciente = self.id_paciente
        obj_hist_clin = Base.Historia_clinica(fecha, descripcion, id_paciente)
        Base.Historia_clinica.guardar_hist_clin(obj_hist_clin)
        self.senial.emit(1)  # Envío la señal a la ventana w_historias_clinicas para que recargue la grilla

    def generar_menu_en_espaniol(self, point):
        menu = QMenu()
        action = menu.addAction("Deshacer")
        action.triggered.connect(self.action_deshacer)
        action = menu.addAction("Rehacer")
        action.triggered.connect(self.action_rehacer)
        menu.addSeparator()
        action = menu.addAction("Cortar")
        action.triggered.connect(self.action_cortar)
        action = menu.addAction("Copiar")
        action.triggered.connect(self.action_copiar)
        action = menu.addAction("Pegar")
        action.triggered.connect(self.action_pegar)
        menu.setStyleSheet("background-color: rgb(255, 255, 255);\n"
                           "selection-color: rgb(255, 255, 255);\n"
                           "color: rgb(0, 0, 0);\n"
                           "selection-background-color: rgb(51, 153, 255);\n"
                           "font: 13pt \"MS Shell Dlg 2\";")
        menu.exec_(self.obj_hist_clin.txt_descripcion.mapToGlobal(point))

    def action_deshacer(self):
        self.obj_hist_clin.txt_descripcion.addAction(self.obj_hist_clin.txt_descripcion.undo())

    def action_rehacer(self):
        self.obj_hist_clin.txt_descripcion.addAction(self.obj_hist_clin.txt_descripcion.redo())

    def action_cortar(self):
        self.obj_hist_clin.txt_descripcion.addAction(self.obj_hist_clin.txt_descripcion.cut())

    def action_copiar(self):
        self.obj_hist_clin.txt_descripcion.addAction(self.obj_hist_clin.txt_descripcion.copy())

    def action_pegar(self):
        self.obj_hist_clin.txt_descripcion.addAction(self.obj_hist_clin.txt_descripcion.paste())
