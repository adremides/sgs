from base_datos import Base
from sqlalchemy import desc
from datetime import datetime

bd = Base()

sesion = bd.session()

ultimo_paciente_agregado = sesion.query(bd.Paciente).order_by(desc('id_paciente')).first()
d = str(int(ultimo_paciente_agregado.documento) + 1)


paciente1 = bd.Paciente('Test ' + d,
                        'Test ' + d,
                        '1978-10-02',
                        d,
                        'Femenino',
                        '',
                        'B',
                        '-',
                        True,
                        'Calle',
                        123,
                        None,
                        '',
                        d,
                        '',
                        'email@me.com',
                        '')

enfermedad = [bd.Enfermedad(datetime.strptime('12/31/2018', '%m/%d/%Y'), "Prueba")]

paciente1.enfermedades = enfermedad

sesion.add(paciente1)

sesion.commit()
sesion.close()